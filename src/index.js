import './index.scss';

const App = {
  id: null,
  processing: false,
};

const inputChanged = () => {
  if (App.input.files.length >= 1) App.uploadBtn.classList.remove('disabled');
};

const checkStatus = () => {
  fetch(`api/status/?id=${App.id}`)
    .then(res => res.json())
    .then((res) => {
      if (res.status === 'complete' || res.status === 'archiving') {
        App.statusbarFill.style.width = '100%';
        App.statusbarText.innerText = '100%';
        App.processing = false;
        App.input.classList.remove('disabled');
        App.downloadBtn.classList.remove('disabled');
        App.downloadBtn.setAttribute('href', `/api/download?id=${App.id}`);
      } else {
        const percent = Math.max(0, Math.min(parseFloat(res.status) * 100, 100));
        App.statusbarFill.style.width = `${percent}%`;
        App.statusbarText.innerText = `${Math.round(percent)}%`;
        setTimeout(checkStatus, 1000);
      }
    });
};

const upload = () => {
  App.input.classList.add('disabled');
  App.statusbar.classList.remove('disabled');
  App.processing = true;
  const formData = new FormData();

  const { files } = App.input;
  for (let i = 0; i < files.length; i += 1) {
    formData.append('images', files[i]);
  }
  App.id = Date.now();
  fetch(`/api/upload?id=${App.id}`, {
    method: 'POST',
    body: formData,
  }).then(checkStatus);
};

const createForm = () => {
  App.form = document.createElement('div');
  App.input = document.createElement('input');
  App.uploadBtn = document.createElement('button');
  App.downloadBtn = document.createElement('a');
  App.statusbar = document.createElement('div');
  App.statusbarFill = document.createElement('div');
  App.statusbarText = document.createElement('div');

  App.form.className = 'form';
  App.uploadBtn.className = 'disabled';
  App.downloadBtn.className = 'disabled';
  App.statusbar.className = 'form-statusbar disabled';
  App.statusbarFill.className = 'form-statusbar-fill';
  App.statusbarText.className = 'form-statusbar-text';
  App.form.setAttribute('method', 'post');
  App.form.setAttribute('enctype', 'multipart/form-data');
  App.input.setAttribute('type', 'file');
  App.input.setAttribute('name', 'images');
  App.input.setAttribute('accept', 'image/png, image/jpeg');
  App.input.setAttribute('multiple', true);
  App.downloadBtn.setAttribute('download', true);
  App.uploadBtn.innerText = 'Upload';

  App.downloadBtn.innerText = 'Download';
  App.input.addEventListener('change', inputChanged);
  App.uploadBtn.addEventListener('click', upload);

  App.statusbar.appendChild(App.statusbarFill);
  App.statusbar.appendChild(App.statusbarText);

  App.form.appendChild(App.input);
  App.form.appendChild(App.uploadBtn);
  App.form.appendChild(App.downloadBtn);
  App.form.appendChild(App.statusbar);
  return App.form;
};

const init = () => {
  App.el = document.querySelector('.app');

  App.el.appendChild(createForm());
};


init();
