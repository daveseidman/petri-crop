import os
import sys
import cv2
import time
# from PIL import Image
# from pytesseract import image_to_string

# Determine if file is Image or Not
# Takes a file path, returns true or false
def isImg(file):
    lwr = file.lower()
    return (lwr.endswith('.png') or lwr.endswith('.jpg') or lwr.endswith('.jpeg'))

# Calculates the nearest odd number (returns n + 1 if n is even)
# Takes an integer
# Returns an integer
def nearestOddNum(num):
    odd = int(num)
    if(odd % 2 == 0):
        odd += 1
    return odd

# uses pytesseract to extract text then combine it into a single string
# Takes a filepath
# Returns a string (can be empty: '')
def getText(path):
    img = Image.open(path)
    img = img.convert('L')
    return image_to_string(img).replace('\n', ' ').replace('\r', '')



def maskCircle(img):
    rows,cols,channels = img.shape

    img2 = cv2.imread('circle.jpg')
    img2 = cv2.resize(img2, (cols, rows))

    roi = img[0:rows, 0:cols]
    # Now create a mask of logo and create its inverse mask also
    img2gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    img1_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
    img2_fg = cv2.bitwise_and(img2,img2,mask = mask)
    dst = cv2.add(img1_bg,img2_fg)
    img[0:rows, 0:cols ] = dst
    return img

# Uses OpenCV's HOUGH_GRADIENT algorithm to find circles in image
# Takes a directory and file
# Returns: currently just saves the file
def findCircle(dir, file):
    path = os.path.join(dir, file)
    colorImg = cv2.imread(path, 1) # color
    if(colorImg is None):
        print '\nnot a valid image:', path
        return

    grayImg = cv2.imread(path, 0) # grayscale
    height, width = grayImg.shape
    if width <= height:
        shortest = width
    else:
        shortest = height

    img2 = cv2.medianBlur(grayImg, nearestOddNum(shortest / 25)) # blurred
    cimg = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR) # 16-bit

    circles = cv2.HoughCircles(img2, cv2.HOUGH_GRADIENT, 1, shortest/2, param1=50, param2=30, minRadius=shortest/7, maxRadius=shortest)

    if(circles is None):
        print '\nno circles found in this image', file
        return

    circle = circles[0][0]
    cropped = colorImg[int(circle[1]-circle[2]):int(circle[1]+circle[2]), int(circle[0]-circle[2]):int(circle[0]+circle[2])]

    masked = maskCircle(cropped)

    outputPath = os.path.join(output, file)
    cv2.imwrite(outputPath, masked)
    result.append(outputPath)

    sys.stdout.flush()
    sys.stdout.write('\r')
    sys.stdout.write(str(len(result) / float(total - 2)))

    # print file, 'text recognized:', getText(path)



if(len(sys.argv) < 2):
    print 'please supply a directory of images'
    exit(0)

dir = sys.argv[1];

if(not os.path.isdir(dir)):
    print 'directory', dir, 'doesn\'t exist'
    exit(0)

output = os.path.join(dir, '_cropped')
result = []
try:
    os.mkdir(output)
except:
    print 'the output directory', output, 'already exists'
    exit(0)

total = len(os.listdir(dir))

for file in os.listdir(dir):
    if(isImg(file)):
        findCircle(dir, file)


exit(0)
