# Crop Circles

![crop circles](https://allthatsinteresting.com/wordpress/wp-content/uploads/2017/07/crop-circles-large-tao.jpg)

Python script to find circles in images of petri dishes and crop based on circles found

## Running  

`python crop.py path/to/images`  

The script will loop through all the images in the given folder and place a cropped
version of each image in a folder called "cropped" inside the supplied folder

## Dependencies

- open cv `pip install opencv-python`  
- tesseract `brew install tesseract`  
- pytesseract `pip install pytesseract`  


## TODO
- improve OCR (try upres, grayscale)
- add param for filename based on OCR or original
- mask outside of circle
- test on very large images
- test on very small images
- test on blurry images
- test on non standard filenames
- test on different image formats (png, bmp, gif, etc)
