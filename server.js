const fs = require('fs');
const express = require('express');
const multer = require('multer');
// const archiver = require('archiver');
const AdmZip = require('adm-zip');
const rimraf = require('rimraf');
const { spawn } = require('child_process');

// Associative array of all jobs
const jobs = {};

const storage = multer.diskStorage({
  destination(req, file, cb) {
    const path = `${__dirname}/images/${req.query.id}`;
    try { fs.statSync(path); } catch (err) { fs.mkdirSync(path); }
    cb(null, path);
  },
  filename(req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });
const app = express();

const deleteFolder = (folder) => {
  rimraf(folder, () => { });
};

app.use(express.static(`${__dirname}/dist`));

app.post('/api/upload', upload.array('images'), (req, res) => {
  jobs[req.query.id] = '0';
  console.log('done, saved', req.files.length, 'files');
  const path = `${__dirname}/images/${req.query.id}`;
  res.send({ status: 'started' });
  const crop = spawn('python', ['crop.py', path]);
  crop.stdout.on('data', (data) => {
    jobs[req.query.id] = data.toString('utf8');
  });
  crop.on('close', () => {
    jobs[req.query.id] = 'archiving';
    const folder = `${__dirname}/images/${req.query.id}`;

    const zip = new AdmZip();
    zip.addLocalFolder(`${folder}/_cropped`);
    zip.writeZip(`${folder}/cropped.zip`, (err) => {
      if (!err) jobs[req.query.id] = 'complete';
    });
    // const output = fs.createWriteStream(`${folder}/cropped.zip`);
    // const archive = archiver('zip');
    // archive.on('close', () => { jobs[req.query.id] = 'complete'; });
    // archive.pipe(output);
    // archive.directory(`${__dirname}/images/${req.query.id}/_cropped`, false);
    // archive.finalize();

    // delete the folder 10 minutes after creation (in case user doesn't download it)
    setTimeout(deleteFolder, 600000, folder);
  });
});

app.get('/api/status', (req, res) => {
  res.send({ status: jobs[req.query.id] });
});


app.get('/api/download', (req, res) => {
  const folder = `${__dirname}/images/${req.query.id}`;
  const path = `${folder}/cropped.zip`;
  res.download(path, 'cropped.zip', (err) => {
    if (!err) deleteFolder(folder);
  });
});

app.get('/*', (req, res) => {
  // TODO: this is a fallback, can setup a 404 page here
  res.sendFile(`${__dirname}/dist/index.html`);
});


app.listen(8000, () => { console.log('app server listening on port 8000'); });
